"use strict"

module.exports = function(gulp, plugins, options, errorHandler) {
  return function() {

    return gulp.src(["src/assets/js/**/*.js", "!src/assets/js/vendor/{angular,angular/**/*}"])
      // Stop gulp from crashing on error
      .pipe(plugins.plumber({
        errorHandler: errorHandler
      }))

      // only change the files that have changed
      .pipe(plugins.cached('babelling'))


      // .pipe(plugins.babel({
      //   modules: "amd"
      // }))

      .pipe(
        plugins.babel({
          presets: [
            [
              '@babel/env',
              {
                modules: 'umd',
              }
            ]
          ],
          plugins: [
            "@babel/plugin-proposal-class-properties"
          ]
        })
      )

      .pipe(plugins.if(options.compress, plugins.uglify()))

      .pipe(gulp.dest(options.paths.build.assets.js))
  }
}
