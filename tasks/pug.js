"use strict"

module.exports = function(gulp, plugins, options, errorHandler, pug) {
  function compilePug(source, destination, cacheName) {
    return gulp.src(source)

      // Handle errors >> stop gulp from crashing on error
      .pipe(plugins.plumber({ errorHandler: errorHandler }))

      //use gulp-cached to only run pug on templates that have changed.
      .pipe(plugins.cached(cacheName))

      // compile pug
      .pipe(plugins.pug({
        pretty: true,
        data: {
          revision: options.revision,
          configPath: "../js/"
        },
    }))
    .on("error", errorHandler)

    // output
    .pipe(gulp.dest(destination))
  }

  /* TODO: Re-map (templates) to templates/ directory
   *       Then uncomment components/ block
   */

  return {
    components: function () {
      // copy components/
      return compilePug(options.paths.dev.components, options.paths.build.components, 'pug-components');
    },
    index: function () {
      // copy index/
      return compilePug(options.paths.dev.index, options.paths.build.root, 'pug-index');
    },
    templates: function () {
      // copy templates/
      return compilePug(options.paths.dev.templates, options.paths.build.root, 'pug-templates');
    }
  };
};
