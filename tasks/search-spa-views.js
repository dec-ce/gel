"use strict"

module.exports = function(gulp, plugins, options, errorHandler, pug) {

  return function() {

    return gulp.src(options.paths.dev.assets.searchSPAViews)

      // Stop gulp from crashing on error
      .pipe(plugins.plumber({
        errorHandler: errorHandler
      }))

      // use gulp-cached to only run pug on templates that have changed.
      .pipe(plugins.cached('pug-templates'))

      // pug
      .pipe(plugins.pug({
        pug: pug,
        pretty: true,
        data: {
          revision: options.revision,
          configPath: "../js/"
        }
      }))

      .on("error", errorHandler)
      .pipe(gulp.dest(options.paths.build.assets.js + "apps/search/views/"))

  }
}
