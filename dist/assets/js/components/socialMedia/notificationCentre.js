(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "./observable"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("./observable"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.observable);
    global.notificationCentre = mod.exports;
  }
})(this, function (_exports, _observable) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports["default"] = void 0;
  _observable = _interopRequireDefault(_observable);

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

  function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

  function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

  function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

  function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

  /**
   * NotificationCentre is a singleton that can be used to broadcasting information across an app
   *
   * @version 1.0
   * @author NSW Department of Education and Communities
   *
   * @class
   * @extends Observable
   */
  var NotificationCentre =
  /*#__PURE__*/
  function (_Observable) {
    _inherits(NotificationCentre, _Observable);

    function NotificationCentre() {
      _classCallCheck(this, NotificationCentre);

      return _possibleConstructorReturn(this, _getPrototypeOf(NotificationCentre).apply(this, arguments));
    }

    _createClass(NotificationCentre, null, [{
      key: "shared",

      /**
       * Returns a pointer to a shared instantiated NotificationCentre object i.e. a singleton
       *
       * @returns {NotificationCentre} NotificationCentre object
       */
      value: function shared() {
        this.instance != null ? this.instance : this.instance = new NotificationCentre();
        return this.instance;
      }
    }]);

    return NotificationCentre;
  }(_observable["default"]);

  var _default = NotificationCentre;
  _exports["default"] = _default;
});