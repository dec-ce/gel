"use strict"

import {
  BOOTSTRAP_UTIL_DISPLAY_NONE,
  GEL_GUIDEDJOURNEY_ROOT,
  GEL_GUIDEDJOURNEY_CONTENT,
  GEL_GUIDEDJOURNEY_PAGE,
  GEL_GUIDEDJOURNEY_ASSET_ID,
  GEL_GUIDEDJOURNEY_SIDENAV,
  GEL_GUIDEDJOURNEY_MOBILENAV,
  GEL_GUIDEDJOURNEY_MOBILENAV_SELECT,
  GEL_GUIDEDJOURNEY_MOBILENAV_FORM,
  GEL_PAGINATION_NEXT,
  GEL_PAGINATION_PREV,
} from '../constants.js';

/**
 * guidedJourney.js - toggles the page content displayed based on the side navigation item selected

 * @class
 * @requires jQuery
 */

class GuidedJourney {
  /**
   * @constructor
   *
   * @param {String}   selector - the jQuery selector where we inject content
   * @param {Object}   config
   * @param {String}   [config.endpoint] - The URL to get child items from
   * @param {Array}    [config.pages] - The key of the variable on the window
   */
  constructor(selector, config = { /* pages, endpoint */ }) {
    this.config = {
      $container : $(selector),
      ...config,
    };

    // Store found elements for later
    this.el = {
      $container:       this.config.$container,
      $content:         this.config.$container.find(GEL_GUIDEDJOURNEY_CONTENT),
      $paginationNext:  this.config.$container.find(GEL_PAGINATION_NEXT),
      $paginationPrev:  this.config.$container.find(GEL_PAGINATION_PREV),
      $sideNav:         this.config.$container.find(GEL_GUIDEDJOURNEY_SIDENAV),
      $mobileNav:       this.config.$container.find(GEL_GUIDEDJOURNEY_MOBILENAV),
      $mobileNavSelect: this.config.$container.find(GEL_GUIDEDJOURNEY_MOBILENAV_SELECT),
      $mobileNavForm:   this.config.$container.find(GEL_GUIDEDJOURNEY_MOBILENAV_FORM),
    };

    this.data = {};

    this.setLoading(true);

    const endpoint = this.config.endpoint;
    Promise.all([
      this.config.pages,
      !!endpoint ? this.fetchNetworkContent(endpoint) : undefined,
    ])
      .then(this.initialise);
  }

  fetchNetworkContent = (endpoint) => {
    return fetch(endpoint)
      .then(res => res.json())
  };

  initialise = ( data ) => {
    const lazyData = data.reduce((acc, val) => {
      return val || acc;
    }, undefined);

    if (!!lazyData) {
      let updatedData;
      const [ firstItem ] = this.getNavItems() || [];

      if (!firstItem || lazyData[0].id === firstItem.href) {
        // Replace
        updatedData = lazyData;
      } else {
        // Prepend
        updatedData = [ firstItem, ...lazyData ];
      }

      // Update the nav items from updated data
      // NOTE!: this needs to be called before you re-assign updatedData.
      this.data.navItems = updatedData.reduce((acc, val) => {
        return [...acc, { href: val.href || val.id, label: val.label }]
      }, []);

      updatedData = updatedData.reduce((acc, val) => {
        return [
          ...acc,
          { ...val, href: `#${val.href || val.id}` },
        ];
      }, []);

      const navItemEls = this.generateSideNavItems(updatedData);
      this.el.$sideNav.html(navItemEls);

      lazyData.forEach((item) => {
        this.injectContentId(item.id, item.content);
      });
    }

    this.populateNavigation();
    this.loadInitialPage();
    this.initialiseContentRegion();
    this.addSkipLinks();
    this.registerEvents();
  };

  addSkipLinks = () => {
    this.el.$content.find('[data-gel-asset-id]').each((index, el) => {
      const pageElement = $(el);
      if (!pageElement.find('a[href="#back_"]').length) {
        pageElement.append('<a class="gel-skiplink__link" href="#back_">Back to menu</a>')
      }
    });
  };

  registerEvents = () => {
    this.el.$paginationNext.on('click', this.onPaginateClick(1));
    this.el.$paginationPrev.on('click', this.onPaginateClick(-1));

    this.el.$mobileNavForm.on('submit', this.handleSubmitMobileNav);

    // Prevent hash change when use back to menu link
    window.addEventListener('hashchange', this.handleHashChange);
  };

  handleSubmitMobileNav = (event) => {
    event.preventDefault();

    const updatedLocation = this.el.$mobileNavSelect.val();

    const { origin, pathname, search } = window.location;
    window.location.replace(`${origin}${pathname}${search}#${updatedLocation}`);

    return false;
  };

  populateNavigation = () => {
    const navItems = this.getNavItems();

    if (!this.getNavItemsFromDOM().length) {
      // Generate side nav from JSON
      this.el.$sideNav.empty();
      this.el.$sideNav.html(
        this.generateMobileNavItems(navItems)
      );
    }

    // Populate mobile nav
    this.el.$mobileNav.empty();
    this.el.$mobileNav.html(
      this.generateMobileNavItems(navItems)
    );
  };

  getCurrentNavItem = () => {
    const currentAssetId = window.location.hash.slice(1);
    const navItems = this.getNavItems();

    return navItems.reduce((acc, val, index) => (
      currentAssetId === val.href && acc.index <= 0
        ? { index, item: val }
        : acc
    ), { index: 0, item: undefined });
  };

  onPaginateClick = (increment) => (event) => {
    event.target.blur(increment);
    this.navigate(increment);
  };

  navigate = (increment) => {
    const navItems = this.getNavItems() || [];
    const currentItem = this.getCurrentNavItem();

    const futureNumber = Math.max(0, currentItem.index + increment);
    const futureItem = navItems[Math.min(futureNumber, navItems.length -1)];

    /* Change location hash */
    if (futureItem) {
      const { origin, pathname, search } = window.location;
      window.location.replace(`${origin}${pathname}${search}#${futureItem.href}`);
    }
  };

  getNavItems = () => {
    if (!this.data.navItems) {
      this.data.navItems = this.getNavItemsFromDOM() || [];
    }
    return [ ...this.data.navItems ];
  };

  setActiveItem = (assetId) => {
    // ACTIVE side nav element
    this.el.$sideNav.find(".active").removeClass('active');
    this.el.$sideNav.find(`[href="#${assetId}"]`).addClass('active');

    // ACTIVE page
    this.handleHidePages();
    const activeEl = this.el.$content.find(`[data-gel-asset-id="${assetId}"]`);
    activeEl.find('.collapse').collapse('hide');

    activeEl.removeClass(BOOTSTRAP_UTIL_DISPLAY_NONE);
    
    // Make the first H2 in the content area focusable by tab
    activeEl.find('h2').first().attr('tabindex', 0);
    
    // ACTIVE mobile nav
    this.el.$mobileNavSelect.val(assetId);

    // Manage Pagination
    const currentItem = this.getCurrentNavItem();
    const navItems = this.getNavItems();

    this.el.$paginationPrev.removeClass(BOOTSTRAP_UTIL_DISPLAY_NONE);
    this.el.$paginationNext.removeClass(BOOTSTRAP_UTIL_DISPLAY_NONE);
    if (currentItem.index <= 0) {
      this.el.$paginationPrev.addClass(BOOTSTRAP_UTIL_DISPLAY_NONE);
    }

    if (currentItem.index >= navItems.length - 1) {
      this.el.$paginationNext.addClass(BOOTSTRAP_UTIL_DISPLAY_NONE);
    }
  };

  getNavItemsFromDOM = () => {
    return this.el.$container
      .find(GEL_GUIDEDJOURNEY_SIDENAV)
      .find('a')
      .map((index, val) => {
        const linkEl = $(val);
        return ({
          href: linkEl.attr('href').slice(1),
          label: linkEl.clone().find('span').remove().end().text(),
        });
      }).toArray();
  };

  generateSideNavItems = (navItems = []) => (
    navItems.reduce((acc, val, index) => {
      const navItem = `
        <li class="sn_parent">
            <a href="${val.href}">
                <span class="sr-only">This is step ${index} of ${navItems.length}</span>
                ${val.label}
            </a>
        </li>
      `;
      return acc + navItem;
    }, '')
  );

  generateMobileNavItems = (navItems = []) => (
    navItems.reduce((acc, val) => {
      const navItem = `
        <option value="${val.href}" aria-label="${val.label}">
          ${val.label}
        </option>
      `;
      return acc + navItem;
    }, '')
  );

  loadInitialPage = () => {
    // Removes the '#' from the string
    const hash = window.location.hash.slice(1);
    const navItems = this.getNavItems() || [];
    const firstItem = navItems[0] || {};
    // Set active page based on hash or the fist item
    this.setActiveItem(hash || firstItem.href || '');
  };

  handleHidePages = () => {
    this.el.$container.find(GEL_GUIDEDJOURNEY_PAGE).addClass(BOOTSTRAP_UTIL_DISPLAY_NONE);
  };

  setLoading = (isLoading = true) => {
    const loadingEl = $('.gel-loader');
    const fnName = isLoading ? 'removeClass' : 'addClass';
    loadingEl[fnName](BOOTSTRAP_UTIL_DISPLAY_NONE);
  };

  findPageEl = (assetId) => (
    this.el.$content.find(
      `${GEL_GUIDEDJOURNEY_PAGE}[${GEL_GUIDEDJOURNEY_ASSET_ID}='${assetId}']`
    )
  );

  injectContentId = (assetId, content) => {
    // Remove any existing page
    const pageEl = this.findPageEl(assetId);
    pageEl.remove();

    // Add a new element to the page
    this.el.$content.append(
      `<div class="${GEL_GUIDEDJOURNEY_PAGE.slice(1)} ${BOOTSTRAP_UTIL_DISPLAY_NONE}" data-gel-asset-id="${assetId}">${content}</div>`
    );

    const contentRegionEl = this.el.$content.find(`[data-gel-asset-id="${assetId}"]`);
    this.initialiseContentRegion(contentRegionEl);
  };

  initialiseContentRegion = () => {};

  getAssetIdFromUrl = (url = '') => {
    const hashLocation = url.indexOf('#');
    if (hashLocation === -1) { return ''; }
    return url.slice(hashLocation + 1);
  };

  handleHashChange = (event) => {
    const { newURL, oldURL } = event;
    const assetId = this.getAssetIdFromUrl(newURL || location.href);
    if (assetId === 'back_') {
      if (oldURL) {
        window.history.replaceState({}, undefined, oldURL);
      }

      if ($('.gel_guided-journey__mobile-nav').css('display') === 'block') {
        console.log(this.el.$container.find('#gel-journey-index-select'));
        this.el.$container.find('#gel-journey-index-select').focus();
        return;
      }

      this.el.$sideNav.find('.active').focus();
      return;
    }

    this.setActiveItem(assetId);
    const activeEl = $(`[data-gel-asset-id="${assetId}"]`);


    $('html, body').animate({
      scrollTop: activeEl.offset().top - 120
    }, 100);

    activeEl.find('h2').first().focus();
  };
}

$(GEL_GUIDEDJOURNEY_ROOT).each(function() {
  const selector = $(this);
  const pages = selector.attr('data-pages');
  const config = {
    pages: pages ? window[pages] : undefined,
    endpoint: selector.attr('data-endpoint'),
  };
  new GuidedJourney(selector, config);
});

// export default GuidedJourney
