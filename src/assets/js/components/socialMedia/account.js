"use strict"

/**
 * Stores social media account details
 *
 * @since 1.1.31
 *
 * @author Digital Services <communications@det.nsw.edu.au>
 * @copyright © 2015 State Government of NSW 2015
 *
 * @class
 */
class Account {

  /**
   * Account constructor
   *
   * @constructor
   *
   * @param {String} service - Social media service. Available options: facebook, twitter, youtube, vimeo, filmpond
   * @param {String} username - The social meida username
   * @param {Number} numberOfPosts - How many posts should be returned by the microservice
   * @param {String} type - Some social media services require an account  "type" to be defined
   *
   * @example
   * // Instantiate a new Account
   * let Account = new Account()
   * // or
   * let Account = new Account("youtube", "digservdoe", 4, "channel")
   */
  constructor(service, username, numberOfPosts, type) {

    this.service = service
    this.username = username
    this.numberOfPosts = numberOfPosts
    this.type = type
    
  }
  

  /**
   * Return the social media account details as a basic Javascript object.
   * 
   * @param {Boolean} mimimumProperties - If set to true only the username and type properties are returned
   * 
   * @returns {Object} - Social media account details
   */
  asObject(mimimumProperties) {
    // If mimimumProperties is set to true only return the 'username' and 'type'
    if (mimimumProperties) {
      return {
        username: this.username,
        type: this.type
      }
    }
    // Return the default set of properties i.e. all the properties
    return {
      service: this.service,
      username: this.username,
      numberOfPosts: this.numberOfPosts,
      type: this.type
    }
  }
  

  /**
   * Return the request paramaters specific to Social Media Microservice expects to recive.
   * 
   * @returns {Object} - Baisc Javascript object with key/value pairs that match the request paramaters specific to Social Media Microservice expects
   */
  toMicroserviceParam() {
    let parameters = {}

    // Depending on the social media service type we need to return a different key/value pairs
    switch (this.service) {
      case "facebook":
        parameters.facebookusername = this.username
        break
      case "twitter":
        parameters.twitterusername = this.username
        break
      case "youtube":
        parameters.youtubeusername = this.username
        parameters.youtubetype = this.type
        break
      case "vimeo":
        parameters.vimeousername = this.username
        parameters.vimeotype = this.type
        break
      case "filmpond":
        parameters.filmpondusername = this.username
        break
    }

    return parameters
  }


  /**
   * Returns a string representation of the Account object
   * 
   * @returns {String} - Account properties as a JSON string
   */
  toString() {
    return JSON.stringify(this.asObject())
  }

}


/**
 * Exports the Account class as a module
 * @module
 */
export default Account
