const postTemplate = `
{{? !it.post.service.match(/error/) }}
  <li>
    <article class="gel-card gel-social-media__card">
      <div class="gel-card__header gel-social-media__header">
        <div class="gel-social-media__avatar">
          <img src="{{=it.post.user.avatar}}" alt="" />
        </div>

        <div class="gel-social-media__name">
          {{=it.post.user.name}}
        </div>
        {{? it.post.service.match(/twitter/) }}
          <div class="gel-social-media__username">
            @{{=it.post.user.username}}
          </div>
        {{?}}            
      </div>

      {{? it.post.service.match(/youtube|vimeo/)}}
        <div class="gel-card__media">
          <iframe  width="360" height="203" title="{{=it.post.id}} video" frameborder="0" src="{{=it.post.embedUrl()}}" frameborder="0" allowfullscreen ></iframe>
        </div>
      {{?}}

      {{? it.post.content.photo && !it.post.service.match(/youtube|vimeo/)}}
        <div class="gel-card__media">
          <img src="{{=it.post.content.photo}}" alt="" />
        </div>
      {{?}}

      <div class="gel-card__content">
        {{? it.post.content.text}}
          <section class="gel-social-media__content">{{=it.post.content.text}}</section>
        {{?}}

        <p>
          <a href="{{=it.post.url}}" class="gel-social-media__post-link">
            View post on {{=it.post.service.capitalize()}}<span class="gel-external-link"></span>
          </a>
        </p>
      </div>

      <div class="gel-card__footer">
        <a href={{=it.post.user.url}} class="gel-social-media__footer-link gel-remove-external-link">
          <i class="fab fa-{{=it.post.service}}" aria-hidden="true" aria-label="{{=it.post.service}} icon"></i>
          <span class="show-on-sr">posted</span>{{=it.moment(it.post.published).fromNow()}}
        </a>
      </div>
    </article>
  </li>
{{?}}
`;

export default postTemplate;