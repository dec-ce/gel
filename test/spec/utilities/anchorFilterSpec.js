"use strict"

var anchorFilterSpec = function(AnchorFilter) {

/* beforeEach(function() {
      loadFixtures("utilities/anchorFilter.html")
    })

*/


  describe("AnchorFilter is initialised", function() {

    var utility,
        selector  = '.gef-a-z-list'

    beforeEach(function() {
      loadFixtures("utilities/anchorFilter.html")
      utility = new AnchorFilter(selector)
    })

    it("expects config to have the values applied", function() {
      expect(utility.config.container).toBe(selector)
    })

    it("expects anchors to have been found", function() {
      expect(utility.anchors.length).not.toBe(0)
    })

    it("expects anchors to have parents which are not the container", function() {
      expect($j("#test-parent").parent()[0]).not.toBe($j('selector')[0])
      expect($j("#test-parent").parent()[0]).toBe($j('[data-character-name="b"]')[0])
    })
  })

  describe("AnchorFilter is initialsed and the container has no anchors", function() {

    var utility,
    selector  = '.has-no-anchors'

    beforeEach(function() {
      loadFixtures("utilities/anchorFilter.html")
    })

    it("throws an error and silently fails", function() {
      expect(function() { new AnchorFilter(selector) }).toThrowError(RangeError)
    })
  })

  describe("AnchorFilter has found anchors", function() {
    var utility,
        selector  = '.gef-a-z-list',
        config    = {
          "inactive_class": "inactive-class"
        }

    beforeEach(function() {
      loadFixtures("utilities/anchorFilter.html")
      utility = new AnchorFilter(selector, config)
    })

    it("applies the inactive class to the anchor", function() {
      expect($j('#has-no-h3')).toHaveClass(config.inactive_class)
    })

    it("has changed the anchor to a span", function() {
      expect($j('#has-no-h3').is('span')).toBe(true)
    })

    it("has removed the href value", function() {
      expect($j("#has-no-h3")).not.toHaveAttr('href')
    })

    it("has not changed an anchor with a matching h3", function() {
      expect($j('#does-not-change').is('a')).toBe(true)
    })
  })
}

define(["/base/dist/assets/js/utilities/anchorFilter.js"], anchorFilterSpec)
